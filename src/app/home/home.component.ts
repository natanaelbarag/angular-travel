import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { ClientsService } from '../dashboard/clients/clients.service';
import { OffersService } from '../dashboard/offers/offers.service';
import { DestinationsService } from '../dashboard/destinations/destinations.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  description: string = '';
  selectedTopic: String = 'offers';

  currentTabModalVisible: boolean = false;

  constructor(
    private clientsService: ClientsService,
    private offersService: OffersService,
    private destinationsService: DestinationsService
  ) {
    this.description = environment.homeDescription;
  }

  ngOnInit(): void {}

  tabSelect(event: any) {
    this.currentTabModalVisible = false;
    this.selectedTopic = event.target.textContent;

    [].slice
      .call(document.getElementsByClassName('button'))
      .forEach((element: any) => {
        element.classList.remove('active');
      });
    event.target.classList.add('active');
  }
}
